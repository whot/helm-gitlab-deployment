#!/bin/bash

set -x
set -e

HELM_VERSION=v3.13.2
KUSTOMIZE_VERSION=v5.2.1
HELMFILE_VERSION=v0.159.0
KUBECTL_VERSION=v1.27.6

curl -LO https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz
tar xvf helm-${HELM_VERSION}-linux-amd64.tar.gz -C /usr/local/bin --strip-components 1 linux-amd64/helm
curl -LO https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize%2F${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz
tar xvf kustomize_${KUSTOMIZE_VERSION}_linux_amd64.tar.gz -C /usr/local/bin kustomize
curl -LO https://github.com/helmfile/helmfile/releases/download/${HELMFILE_VERSION}/helmfile_${HELMFILE_VERSION/v/}_linux_amd64.tar.gz
tar xvf helmfile_${HELMFILE_VERSION/v/}_linux_amd64.tar.gz -C /usr/local/bin helmfile

helmfile init --force

helm repo add gitlab https://charts.gitlab.io

curl -LO "https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl"
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
rm kubectl

pip install yq
