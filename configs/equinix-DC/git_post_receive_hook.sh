#!/bin/bash

export REMOTE_REPO="$(git config --local fdo.mirror-dir)"

export GIT_SSH_COMMAND="ssh -o 'ClearAllForwardings yes' -o 'GlobalKnownHostsFile /dev/null' -o 'IdentitiesOnly yes' -o 'IdentityFile /gitlab-ssh-keys/kemper-ssh-priv' -o 'StrictHostKeyChecking yes' -o 'HashKnownHosts no' -o 'UserKnownHostsFile /gitlab-ssh-keys/kemper-ssh-known-hosts' -F /dev/null"

while read oldrev newrev refname; do
  case "$refname" in
    refs/heads/*)
      git push ssh://gitlab-mirror@kemper.freedesktop.org/git/${REMOTE_REPO} +${newrev}:${refname} >/dev/null
      ;;
    refs/tags/*)
      git push ssh://gitlab-mirror@kemper.freedesktop.org/git/${REMOTE_REPO} +${refname} >/dev/null
      ;;
    *)
      ;;
  esac
done
